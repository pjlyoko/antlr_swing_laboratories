package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected Integer add(Integer a, Integer b) {
        return a + b;
	}
	
	protected Integer sub(Integer a, Integer b) {
	        return a - b;
	}
	
	protected Integer mul(Integer a, Integer b) {
	        return a * b;
	}
	
	// TODO: a / 0
	protected Integer div(Integer a, Integer b) {
	        return a / b;
	}


	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void declareVar(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected void setVar(String name, Integer value) throws RuntimeException {
		try {
			globalSymbols.setSymbol(name, value);
		} catch(Exception e) {
			throw new RuntimeException("Zmienna " + name + " nie istnieje!");
		}
	}
	
	protected Integer getVar(String name) throws RuntimeException {
		try {
			Integer x = globalSymbols.getSymbol(name);
			return x;
		} catch(Exception e) {
			throw new RuntimeException("Zmienna " + name + " nie istnieje!");
		}
	}
}
