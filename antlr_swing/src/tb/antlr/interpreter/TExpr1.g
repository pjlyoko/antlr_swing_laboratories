tree grammar TExpr1;

options {
  tokenVocab = Expr;           // gdzie szukać tokenów
  ASTLabelType = CommonTree;
  superClass = MyTreeParser; // jaki pakiet ma być rozszerzany
}

@header {
  package tb.antlr.interpreter;
}

// drukuj() pochodzi z MyTreeParser
prog    : ( e=expr {drukuj ($e.text + " = " + $e.out.toString());}
          | d=dekl {drukuj ("> " + $d.text);}
          | s=set  {drukuj ("> " + $s.var + " = " + $s.num.toString());}
          )* ;

expr returns [Integer out]
        : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVar($ID.text);}
        ;

dekl
        : ^(VAR   i1=ID          ) {declareVar($i1.text);}
        ;

set returns [String var, Integer num]
        : ^(PODST i1=ID   e2=expr) {setVar($i1.text, $e2.out); $var = $i1.text; $num = $e2.out;}
        ;
